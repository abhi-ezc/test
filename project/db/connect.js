const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/sportsbetting', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then((data) => {
    console.log("connected to db");
})

module.exports = mongoose;