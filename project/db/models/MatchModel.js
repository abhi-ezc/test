const mongoose = require("../connect");
var schema = mongoose.Schema;
let matchSchema = new schema({
  MatchName: String,
  TeamA: String,
  TeamB: String,
  StartTime: Number,
  Index: Number,
  Address: String,
  Status: String,
});
let MatchData = mongoose.model("Match-data", matchSchema);
module.exports = MatchData;
