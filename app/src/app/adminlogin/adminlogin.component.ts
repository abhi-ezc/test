import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  public uname: String;
  public password: String;

  constructor(
    public adminService: AdminService,
    public router:Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('isLogin') == "true")
    {
      this.router.navigate(['/admin/home']);
    }
  }

  onAdminLogin()
  {
    this.adminService.checkLogin(this.uname, this.password).subscribe((data) => {
      if (data == true)
      {
        localStorage.setItem('isLogin', 'true');
        this.router.navigate(['/admin/home']);
      }
      else
      {
        localStorage.clear();
        alert('wrong credentials');
        }
      
    }) 
  }

}
