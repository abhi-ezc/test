import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminaddbetinfoComponent } from './adminaddbetinfo/adminaddbetinfo.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  {
    path: 'admin',
    component:AdminloginComponent
  },
  {
    path: 'admin/home',
    component:AdminhomeComponent
  },
  {
    path: "admin/addbet",
    component:AdminaddbetinfoComponent
  },
  {
    path: "**",
    component:NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
